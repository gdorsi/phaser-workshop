# Arcademy phaser workshop

Ciao a tutti!

Questo è il repository del workshop su Phaser della nostra _Arcademy_!


## Setup

L'unico requisito del progetto è un browser (Chrome o Firefox) e un web server.

Sui sitemi UNIX-like con python3 installato basta eseguire lo script `run-server` presente in questo repository.

Se `run-server` non vi funziona un qualsiasi altro web server va bene. 
Anni fa utilizzavo `XAMPP` anche se immagino ci siano soluzioni più leggere attualmente.

Se avete un browser Firefox non c'è necessità di utilizzare un web server.

## Useful links

La fonte migliore di documentazione che ho trovato è [questo sito](https://rexrainbow.github.io/phaser3-rex-notes/docs/site/) che praticamente funge da documentazione non ufficiale.

La [documentazione](https://photonstorm.github.io/phaser3-docs/index.html) di phaser non è delle migliori (autogenerata e alquanto inutile), ma ci sono un sacco di [tutorial](https://phaser.io/news/category/tutorial) in giro.

Attenzione a non finire in tutorial/documentazione di Phaser 2 (la versione vecchia) perchè con Phaser 3 è cambiata molta roba.

## Credits

Per il codice di questo workshop ho preso spunto dalla serie di articoli di [Emanuele Feronato](https://www.emanueleferonato.com/tag/endless-runner/)
