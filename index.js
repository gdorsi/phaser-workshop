import "https://unpkg.com/phaser@3.18.1/dist/phaser.min.js";

// global game options
const GAME_OPTS = {
};

class MainScene extends Phaser.Scene {
  constructor() {
    super("MainScene");
  }

  //Called right before start, used to download all the assets
  preload() {
    this.load.image("platform", "platform.png");
    this.load.image("player", "player.png");
  }

  //The scene creation hook
  create() {
  }

  //The scene update loop
  update() {
  }
}

window.onload = function() {
  new Phaser.Game({
    type: Phaser.AUTO, //Rendering type, tries to use webgl when possible
    width: 667,
    height: 375,
    scene: MainScene,
    parent: document.body,
    backgroundColor: 0x87CEEB,

    // physics settings
    physics: {
      default: "arcade"
    }
  });
};
