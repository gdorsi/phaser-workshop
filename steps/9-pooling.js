import "https://unpkg.com/phaser@3.18.1/dist/phaser.min.js";

// global game options
const GAME_OPTS = {
  playerStartPosition: 100,
  playerGravity: 900,
  platformStartSpeed: 350,
  jumpForce: 400,
  platformSizeRange: [50, 250],
  spawnRange: [100, 350]
};

class MainScene extends Phaser.Scene {
  constructor() {
    super("MainScene");
  }

  //Called right before start, used to download all the assets
  preload() {
    this.load.image("platform", "platform.png");
    this.load.image("player", "player.png");
  }

  //The scene creation hook
  create() {
    const { game } = this.sys;

    // group with all active platforms.
    this.platformGroup = this.add.group({
      // once a platform is removed, it's added to the pool
      removeCallback(platform) {
        platform.scene.platformPool.add(platform);
      }
    });

    // pool
    this.platformPool = this.add.group({
      // once a platform is removed from the pool, it's added to the active platforms group
      removeCallback(platform) {
        platform.scene.platformGroup.add(platform);
      }
    });

    // adding the player;
    this.player = this.physics.add.sprite(
      GAME_OPTS.playerStartPosition, //x
      game.config.height / 2, //y
      "player" //name
    );

    this.player.setGravityY(GAME_OPTS.playerGravity);

    this.addPlatform(game.config.width, game.config.width / 2);

     // setting collisions between the player and the platform group
     this.physics.add.collider(this.player, this.platformGroup, () => {
      this.jumps = 0;
    });

    this.jumps = 2; //No jump on start

    //Avoids memory leaks on restart
    if (this.jumpButton) this.jumpButton.destroy(); 
    this.jumpButton = this.input.keyboard.addKey('SPACE'); 
    this.jumpButton.on('up', this.jump, this);
  }

  addPlatform(platformWidth, posX) {
    const { game } = this.sys;

    this.nextPlatformDistance = Phaser.Math.Between(...GAME_OPTS.spawnRange);

    if (this.platformPool.getLength()) {
      const platform = this.platformPool.getFirst();

      platform.x = posX;
      platform.active = true;
      platform.visible = true;
      platform.displayWidth = platformWidth;
      
      this.platformPool.remove(platform);
      return;
    } 

    const platform = this.physics.add.sprite(
      posX,
      game.config.height * 0.8,
      "platform"
    );

    platform.setImmovable(true);
    platform.setVelocityX(GAME_OPTS.platformStartSpeed * -1);
    platform.setFrictionX(0);
    platform.displayWidth = platformWidth;

    this.platformGroup.add(platform);
  }

  jump() {
    if (this.jumps >= 2) return;
    
    this.jumps++

    this.player.setVelocityY(GAME_OPTS.jumpForce * -1);
  }
  
  //The scene update loop
  update() {
    const { game } = this.sys;

    // game over
    if (this.player.y > game.config.height) {
      this.scene.start("MainScene");
    }

    const gameWidth = game.config.width;
    let minDistance = gameWidth;

    this.platformGroup.getChildren().forEach(platform => {
      const platformRightX = platform.x + platform.displayWidth / 2;
      const platformDistance = gameWidth - platformRightX;

      minDistance = Math.min(minDistance, platformDistance);

      //The platform is out of screen
      if (platformRightX < 0) {
        this.platformGroup.killAndHide(platform);
        this.platformGroup.remove(platform);
      }
    });

    // adding new platforms
    if (minDistance > this.nextPlatformDistance) {
      const nextPlatformWidth = Phaser.Math.Between(
        ...GAME_OPTS.platformSizeRange
      );

      this.addPlatform(
        nextPlatformWidth,
        game.config.width + nextPlatformWidth / 2
      );
    }
  }
}

window.onload = function() {
  new Phaser.Game({
    type: Phaser.AUTO, //Rendering type, tries to use webgl when possible
    width: 667,
    height: 375,
    scene: MainScene,
    parent: document.body,
    backgroundColor: 0x87CEEB,

    // physics settings
    physics: {
      default: "arcade"
    }
  });
};
