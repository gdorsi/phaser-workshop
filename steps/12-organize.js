import "https://unpkg.com/phaser@3.18.1/dist/phaser.min.js";

// global game options
const GAME_OPTS = {
  playerStartPosition: 100,
  playerGravity: 900,
  platformStartSpeed: 350,
  jumpForce: 400,
  platformSizeRange: [50, 250],
  spawnRange: [100, 350],
  firePercent: 25
};

class PreloadGame extends Phaser.Scene {
  //Called right before start, used to download all the assets
  preload() {
    this.load.image("platform", "platform.png");

    // player is a sprite sheet made by 24x48 pixels
    this.load.spritesheet("player", "sprites/player.png", {
      frameWidth: 24,
      frameHeight: 48
    });

    // the firecamp is a sprite sheet made by 32x58 pixels
    this.load.spritesheet("fire", "sprites/fire.png", {
      frameWidth: 40,
      frameHeight: 70
    });
  }

  //The scene creation hook
  create() {
    // setting player animation
    this.anims.create({
      key: "run",
      frames: this.anims.generateFrameNumbers("player", {
        start: 0,
        end: 1
      }),
      frameRate: 8,
      repeat: -1
    });

    // setting fire animation
    this.anims.create({
      key: "burn",
      frames: this.anims.generateFrameNumbers("fire", {
        start: 0,
        end: 4
      }),
      frameRate: 15,
      repeat: -1
    });

    this.scene.start("MainScene");
  }
}

class Player extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, "player");

    scene.physics.world.enable(this);
    scene.add.existing(this);

    this.dying = false;
    this.body.setGravityY(GAME_OPTS.playerGravity);

    this.jumps = 2; //No jump on start
    this.jumpButton = scene.input.keyboard.addKey("SPACE");
    this.jumpButton.on("up", this.jump, this);

    // setting collisions between the player and the platform group
    this.platformCollider = scene.physics.add.collider(
      this,
      scene.platformGroup,
      () => {
        this.run();
        this.jumps = 0;
      }
    );

    // setting collisions between the player and the fire group
    scene.physics.add.overlap(
      this,
      scene.fireGroup,
      () => {
        this.burn();
        scene.physics.world.removeCollider(this.platformCollider);
      }
    );
  }

  destroy() {
    this.jumpButton.destroy();
  }

  run() {
    if (!this.anims.isPlaying) {
      this.anims.play("run");
    }
  }

  burn() {
    this.dying = true;
    this.anims.stop();
    this.setFrame(2);
    this.body.setVelocityY(-200);
  }

  jump() {
    if (this.dying || this.jumps >= 2) return;
    
    this.jumps++

    this.body.setVelocityY(GAME_OPTS.jumpForce * -1);

    // stops animation
    this.anims.stop();
    this.setFrame(0);
  }
}

class MainScene extends Phaser.Scene {
  constructor() {
    super("MainScene");
  }

  //The scene creation hook
  create() {
    const { game } = this.sys;

    // group with all active platforms.
    this.platformGroup = this.add.group({
      // once a platform is removed, it's added to the pool
      removeCallback(platform) {
        platform.scene.platformPool.add(platform);
      }
    });

    // pool
    this.platformPool = this.add.group({
      // once a platform is removed from the pool, it's added to the active platforms group
      removeCallback(platform) {
        platform.scene.platformGroup.add(platform);
      }
    });

    // group with all active firecamps.
    this.fireGroup = this.add.group({
      // once a firecamp is removed, it's added to the pool
      removeCallback: function(fire) {
        fire.scene.firePool.add(fire);
      }
    });

    // fire pool
    this.firePool = this.add.group({
      // once a fire is removed from the pool, it's added to the active fire group
      removeCallback: function(fire) {
        fire.scene.fireGroup.add(fire);
      }
    });

    // adding the player;
    this.player = new Player(
      this,
      GAME_OPTS.playerStartPosition, //x
      game.config.height / 2 //y
    );

    this.addPlatform(game.config.width, game.config.width / 2, true);
  }

  addPlatform(platformWidth, posX, skipFire) {
    const { game } = this.sys;

    this.nextPlatformDistance = Phaser.Math.Between(...GAME_OPTS.spawnRange);

    const posY = game.config.height * 0.8;
    let platform;

    if (this.platformPool.getLength()) {
      platform = this.platformPool.getFirst();

      platform.x = posX;
      platform.active = true;
      platform.visible = true;

      this.platformPool.remove(platform);
    } else {
      platform = this.physics.add.sprite(posX, posY, "platform");

      platform.setImmovable(true);
      platform.setVelocityX(GAME_OPTS.platformStartSpeed * -1);
      platform.setFrictionX(0);

      this.platformGroup.add(platform);
    }

    platform.displayWidth = platformWidth;

    // is there a fire over the platform?
    if (Phaser.Math.Between(1, 100) <= GAME_OPTS.firePercent && !skipFire) {
      if (this.firePool.getLength()) {
        const fire = this.firePool.getFirst();
        fire.x =
          posX - platformWidth / 2 + Phaser.Math.Between(1, platformWidth);
        fire.y = posY - 46;
        fire.alpha = 1;
        fire.active = true;
        fire.visible = true;
        this.firePool.remove(fire);
      } else {
        const fire = this.physics.add.sprite(
          posX - platformWidth / 2 + Phaser.Math.Between(1, platformWidth),
          posY - 46,
          "fire"
        );
        fire.setImmovable(true);
        fire.setVelocityX(platform.body.velocity.x);
        fire.setSize(8, 2, true);
        fire.anims.play("burn");
        this.fireGroup.add(fire);
      }
    }
  }

  //The scene update loop
  update() {
    const { game } = this.sys;

    // game over
    if (this.player.y > game.config.height) {
      this.scene.start("MainScene");
    }

    const gameWidth = game.config.width;
    let minDistance = gameWidth;

    this.platformGroup.getChildren().forEach(platform => {
      const platformRightX = platform.x + platform.displayWidth / 2;
      const platformDistance = gameWidth - platformRightX;

      minDistance = Math.min(minDistance, platformDistance);

      //The platform is out of screen
      if (platformRightX < 0) {
        this.platformGroup.killAndHide(platform);
        this.platformGroup.remove(platform);
      }
    });

    // recycling fire
    this.fireGroup.getChildren().forEach(function(fire) {
      if (fire.x < -fire.displayWidth / 2) {
        this.fireGroup.killAndHide(fire);
        this.fireGroup.remove(fire);
      }
    }, this);

    // adding new platforms
    if (minDistance > this.nextPlatformDistance) {
      const nextPlatformWidth = Phaser.Math.Between(
        ...GAME_OPTS.platformSizeRange
      );

      this.addPlatform(
        nextPlatformWidth,
        game.config.width + nextPlatformWidth / 2
      );
    }
  }
}

window.onload = function() {
  new Phaser.Game({
    type: Phaser.AUTO, //Rendering type, tries to use webgl when possible
    width: 667,
    height: 375,
    scene: [PreloadGame, MainScene],
    parent: document.body,
    backgroundColor: 0x87ceeb,

    // physics settings
    physics: {
      default: "arcade"
    }
  });
};
