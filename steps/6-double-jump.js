import "https://unpkg.com/phaser@3.18.1/dist/phaser.min.js";

// global game options
const GAME_OPTS = {
  playerStartPosition: 100,
  playerGravity: 900,
  platformStartSpeed: 350,
  jumpForce: 400,
};

class MainScene extends Phaser.Scene {
  constructor() {
    super("MainScene");
  }

  //Called right before start, used to download all the assets
  preload() {
    this.load.image("platform", "platform.png");
    this.load.image("player", "player.png");
  }

  //The scene creation hook
  create() {
    const { game } = this.sys;

    // group with all active platforms.
    this.platformGroup = this.add.group();

    // adding the player;
    this.player = this.physics.add.sprite(
      GAME_OPTS.playerStartPosition, //x
      game.config.height / 2, //y
      "player" //name
    );

    this.player.setGravityY(GAME_OPTS.playerGravity);

    this.addPlatform(game.config.width, game.config.width / 2);

    // setting collisions between the player and the platform group
    this.physics.add.collider(this.player, this.platformGroup, () => {
      this.jumps = 0;
    });

    this.jumps = 2; //No jump on start

    //Avoids memory leaks on restart
    if (this.jumpButton) this.jumpButton.destroy(); 
    this.jumpButton = this.input.keyboard.addKey('SPACE'); 
    this.jumpButton.on('up', this.jump, this);
  }

  addPlatform(platformWidth, posX) {
    const { game } = this.sys;

    const platform = this.physics.add.sprite(
      posX,
      game.config.height * 0.8,
      "platform"
    );

    platform.setImmovable(true);
    platform.setVelocityX(GAME_OPTS.platformStartSpeed * -1);
    platform.setFrictionX(0);
    platform.displayWidth = platformWidth;

    this.platformGroup.add(platform);
  }

  jump() {
    if (this.jumps >= 2) return;
    
    this.jumps++

    this.player.setVelocityY(GAME_OPTS.jumpForce * -1);
  }

  //The scene update loop
  update() {
    const { game } = this.sys;

    // game over
    if (this.player.y > game.config.height) {
      this.scene.start("MainScene");
    }
  }
}

window.onload = function() {
  new Phaser.Game({
    type: Phaser.AUTO, //Rendering type, tries to use webgl when possible
    width: 667,
    height: 375,
    scene: MainScene,
    parent: document.body,
    backgroundColor: 0x87CEEB,

    // physics settings
    physics: {
      default: "arcade"
    }
  });
};
