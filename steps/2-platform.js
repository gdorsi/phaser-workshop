import "https://unpkg.com/phaser@3.18.1/dist/phaser.min.js";

// global game options
const GAME_OPTS = {
  playerStartPosition: 100,
  playerGravity: 900,
};

class MainScene extends Phaser.Scene {
  constructor() {
    super("MainScene");
  }

  //Called right before start, used to download all the assets
  preload() {
    this.load.image("platform", "platform.png");
    this.load.image("player", "player.png");
  }

  //The scene creation hook
  create() {
    const { game } = this.sys;

    // adding the player;
    this.player = this.physics.add.sprite(
      GAME_OPTS.playerStartPosition, //x
      game.config.height / 2, //y
      "player" //name
    );

    this.player.setGravityY(GAME_OPTS.playerGravity);

    this.addPlatform(game.config.width, game.config.width / 2);
  }

  addPlatform(platformWidth, posX) {
    const { game } = this.sys;

    const platform = this.physics.add.sprite(
      posX,
      game.config.height * 0.8,
      "platform"
    );

    platform.displayWidth = platformWidth;
  }

  //The scene update loop
  update() {}
}

window.onload = function() {
  new Phaser.Game({
    type: Phaser.AUTO, //Rendering type, tries to use webgl when possible
    width: 667,
    height: 375,
    scene: MainScene,
    parent: document.body,
    backgroundColor: 0x87CEEB,

    // physics settings
    physics: {
      default: "arcade"
    }
  });
};
